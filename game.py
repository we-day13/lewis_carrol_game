initial_word= 'CLAY'
final_word = 'GOLD'

WORDS = [word for word in open("sowpods.txt")]

def word_ladder(initial_word: str, final_word: str):
    for i, ch in enumerate(final_word):
        initial_word = initial_word[:i] + ch + initial_word[i + 1:]

